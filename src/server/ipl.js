module.exports = {
  //MatchesPlayed per year

  matchesPlayedPerYear: function (matches) {
    const matchesPerYear = matches.reduce((matchesPerYear, currmatch) => {
      if (matchesPerYear[currmatch.season]) {
        matchesPerYear[currmatch.season] += 1;
      } else {
        matchesPerYear[currmatch.season] = 1;
      }
      return matchesPerYear;
    }, {});
    return matchesPerYear;
  },

  //matches won peryear by every team

  matchesWonPerTeamPerYear: function (matches) {
    const matchesWonPerYear = matches.reduce((matchesWonPerYear, curr) => {
      if (!matchesWonPerYear[curr.winner]) {
        matchesWonPerYear[curr.winner] = {};
      }
      if (matchesWonPerYear[curr.winner][curr.season]) {
        matchesWonPerYear[curr.winner][curr.season] += 1;
      } else {
        matchesWonPerYear[curr.winner][curr.season] = 1;
      }
      return matchesWonPerYear;
    }, {});
    return matchesWonPerYear;
  },

  //Extra runs conceeded per team in year 2016

  extraRunsPerTeamIn2016: function (matches, deliveries) {
    const matchIdOf2016 = matches.reduce((matchIdOf2016, curr) => {
      if (curr.season === "2016") {
        matchIdOf2016.push(curr.id);
      }
      return matchIdOf2016;
    }, []);

    const extraRunsConcededPerTeam = deliveries.reduce(
      (extraRunsConcededPerTeam, curr) => {
        if (matchIdOf2016.includes(curr.match_id)) {
          if (extraRunsConcededPerTeam[curr.bowling_team]) {
            extraRunsConcededPerTeam[curr.bowling_team] += Number(
              curr.extra_runs
            );
          } else {
            extraRunsConcededPerTeam[curr.bowling_team] = Number(
              curr.extra_runs
            );
          }
        }
        return extraRunsConcededPerTeam;
      },
      {}
    );

    return extraRunsConcededPerTeam;
  },

  //Top 10 Economical Bowlers in 2015

  economicalBowlersIn2015: function (matches, deliveries) {
    const matchesIdIn2015 = matches.reduce((matchesIdIn2015, curr) => {
      if (curr.season === "2015") {
        matchesIdIn2015.push(curr.id);
      }
      return matchesIdIn2015;
    }, []);

    const runsConceded = deliveries.reduce((runsConceded, curr) => {
      if (matchesIdIn2015.includes(curr.match_id)) {
        if (runsConceded[curr.bowler]) {
          runsConceded[curr.bowler] += +curr.total_runs;
        } else {
          runsConceded[curr.bowler] = +curr.total_runs;
        }
      }
      return runsConceded;
    }, []);

    const ballsbowled = deliveries.reduce((ballsbowled, curr) => {
      if (matchesIdIn2015.includes(curr.match_id)) {
        if (
          ballsbowled[curr.bowler] &&
          curr.noball_runs === "0" &&
          curr.wide_runs === "0"
        ) {
          ballsbowled[curr.bowler] += 1;
        } else {
          if (curr.noball_runs === "0" && curr.wide_runs === "0") {
            ballsbowled[curr.bowler] = 1;
          }
        }
      }
      return ballsbowled;
    }, []);

    const economiesArray = [];
    for (let i in runsConceded) {
      let b = (runsConceded[i] / ballsbowled[i]) * 6;
      economiesArray.push([i, b]);
    }

    economiesArray.sort((curr, next) => curr[1] - next[1]);

    let top10EconomicalBowlers = [];

    for (let i = 0; i < 10; i++) {
      top10EconomicalBowlers.push([
        {
          rank: i + 1,
          bowler: economiesArray[i][0],
          Economy: economiesArray[i][1],
        },
      ]);
    }

    return top10EconomicalBowlers;
  },

  //Number of times each team won both toss and match

  tossAndMatchWinPerTeam: function (matches) {
    const tossAndMatchWinPerTeam = matches.reduce(
      (tossAndMatchWinPerTeam, curr) => {
        if (
          tossAndMatchWinPerTeam[curr.winner] &&
          tossAndMatchWinPerTeam[curr.winner] ===
            tossAndMatchWinPerTeam[curr.toss_winner]
        ) {
          tossAndMatchWinPerTeam[curr.winner] += 1;
        } else {
          if (
            tossAndMatchWinPerTeam[curr.winner] ===
            tossAndMatchWinPerTeam[curr.toss_winner]
          ) {
            tossAndMatchWinPerTeam[curr.winner] = 1;
          }
        }
        return tossAndMatchWinPerTeam;
      },
      {}
    );
    return tossAndMatchWinPerTeam;
  },

  //Most Economical Bowler in super overs

  economicalBowlerInSuperOver: function (matches, deliveries) {
    const superOvers = deliveries.filter((superOvers) => {
      return superOvers.is_super_over != 0;
    });

    const runsGiven = superOvers.reduce((runsGiven, curr) => {
      if (runsGiven[curr.bowler]) {
        runsGiven[curr.bowler] += +curr.total_runs;
      } else {
        runsGiven[curr.bowler] = +curr.total_runs;
      }
      return runsGiven;
    }, []);

    const numberOfBallsBowled = superOvers.reduce(
      (numberOfBallsBowled, curr) => {
        if (
          numberOfBallsBowled[curr.bowler] &&
          curr.wide_runs === "0" &&
          curr.noball_runs === "0"
        ) {
          numberOfBallsBowled[curr.bowler] += 1;
        } else {
          if (curr.wide_runs === "0" && curr.noball_runs === "0") {
            numberOfBallsBowled[curr.bowler] = 1;
          }
        }
        return numberOfBallsBowled;
      },
      []
    );

    let superOverEconomies = [];
    for (let i in runsGiven) {
      let e = (runsGiven[i] / numberOfBallsBowled[i]) * 6;
      superOverEconomies.push([i, e]);
    }
    superOverEconomies.sort((curr, next) => curr[1] - next[1]);

    return `The most economical bowler in super overs is ${superOverEconomies[0][0]} with Economy rate of ${superOverEconomies[0][1]} rpo `;
  },

  //Strikerate of a batsman(Virat Kohli) per season

  strikerateOfABatsmanPerSeason: function (matches, deliveries) {
    const deliveriesPlayedByKohli = deliveries.filter(
      (deliveriesPlayedByKohli) => {
        return deliveriesPlayedByKohli.batsman === "V Kohli";
      }
    );

    matchIdsOfEachSeason = matches.reduce((matchIdsOfEachSeason, curr) => {
      if (!matchIdsOfEachSeason[curr.season]) {
        matchIdsOfEachSeason[curr.season] = [];
      }
      if (matchIdsOfEachSeason[curr.season]) {
        matchIdsOfEachSeason[curr.season].push(curr.id);
      }
      return matchIdsOfEachSeason;
    }, {});

    const runsScoredByKohliPerSeason = deliveriesPlayedByKohli.reduce(
      (runsScoredByKohliPerSeason, curr) => {
        let x = 0;
        for (let i in matchIdsOfEachSeason) {
          if (matchIdsOfEachSeason[i].includes(curr.match_id)) {
            x = i;
          }
        }
        if (runsScoredByKohliPerSeason[x]) {
          runsScoredByKohliPerSeason[x] += +curr.batsman_runs;
        } else {
          runsScoredByKohliPerSeason[x] = +curr.batsman_runs;
        }
        return runsScoredByKohliPerSeason;
      },
      {}
    );

    const ballsFacedByKohliPerSeason = deliveriesPlayedByKohli.reduce(
      (ballsFacedByKohliPerSeason, curr) => {
        let x = 0;
        for (let i in matchIdsOfEachSeason) {
          if (matchIdsOfEachSeason[i].includes(curr.match_id)) {
            x = i;
          }
        }
        if (ballsFacedByKohliPerSeason[x] && curr.wide_runs === "0") {
          ballsFacedByKohliPerSeason[x] += 1;
        } else {
          if (curr.wide_runs === "0") {
            ballsFacedByKohliPerSeason[x] = 1;
          }
        }
        return ballsFacedByKohliPerSeason;
      },
      {}
    );

    const strikerateOfKohliPerSeason = [];
    for (let season in runsScoredByKohliPerSeason) {
      const strikerate = (
        (runsScoredByKohliPerSeason[season] /
          ballsFacedByKohliPerSeason[season]) *
        100
      ).toFixed(2);
      strikerateOfKohliPerSeason.push({
        season: season,
        strikerate: strikerate,
      });
    }
    return strikerateOfKohliPerSeason;
  },

  // Player with most number of Man of the Match awards in each season

  playerWithMostMOMPerSeason: function (matches) {
    const mom = matches.reduce((mom, curr) => {
      if (!mom[curr.season]) {
        mom[curr.season] = [];
      }
      if (mom[curr.season][curr.player_of_match]) {
        mom[curr.season][curr.player_of_match] += 1;
      } else {
        mom[curr.season][curr.player_of_match] = 1;
      }
      return mom;
    }, {});

    let playersWithMostMomPerSeason = [];
    let allPlayersWithMom = [];

    for (let i in mom) {
      let allEntries = [];
      for (let j in mom[i]) {
        allEntries.push([i, j, mom[i][j]]);
      }
      allEntries.sort((curr, next) => next[2] - curr[2]);
      allPlayersWithMom.push(allEntries);
    }

    for (let i in allPlayersWithMom) {
      playersWithMostMomPerSeason.push(allPlayersWithMom[i][0]);
    }
    return playersWithMostMomPerSeason;
  },

  //Highest Number of Times a bowler dismmised same batsman

  highestNumberOfTimesSameBatsmanAndBowlerInvolvedInDismissal: function (
    matches,
    deliveries
  ) {
    const allDismissals = deliveries.reduce((allDismissals, curr) => {
      if (!allDismissals[curr.batsman]) {
        allDismissals[curr.batsman] = [];
      }
      if (curr.player_dismissed !== "" && curr.dismissal_kind !== "run out") {
        if (allDismissals[curr.batsman][curr.bowler]) {
          allDismissals[curr.batsman][curr.bowler] += 1;
        } else {
          allDismissals[curr.batsman][curr.bowler] = 1;
        }
      }
      return allDismissals;
    }, {});

    let bowlerAndBatsmanDismissalals = [];

    for (let i in allDismissals) {
      for (let j in allDismissals[i]) {
        bowlerAndBatsmanDismissalals.push([i, j, allDismissals[i][j]]);
      }
    }
    bowlerAndBatsmanDismissalals.sort((curr, next) => next[2] - curr[2]);
    return {
      Batsman: bowlerAndBatsmanDismissalals[0][0],
      Bowler: bowlerAndBatsmanDismissalals[0][1],
      "Number of times": bowlerAndBatsmanDismissalals[0][2],
    };
  },
};
