const fs = require("fs");

const path = require("path");

const ipl = require("./ipl.js");

const matches = require(path.join(__dirname, "../data/matches.json"));

const deliveries = require(path.join(__dirname, "../data/deliveries.json"));

//function calls

createOutputFile(
  matches,
  deliveries,
  "matchesperyear",
  ipl.matchesPlayedPerYear
);
createOutputFile(
  matches,
  deliveries,
  "matcheswonperteamperyear",
  ipl.matchesWonPerTeamPerYear
);
createOutputFile(
  matches,
  deliveries,
  "extrarunsperteamin2016",
  ipl.extraRunsPerTeamIn2016
);
createOutputFile(
  matches,
  deliveries,
  "top10economicalbowlersin2015",
  ipl.economicalBowlersIn2015
);
createOutputFile(
  matches,
  deliveries,
  "tossandmatchwinspercreateOutputFilematches,delteam",
  ipl.tossAndMatchWinPerTeam
);
createOutputFile(
  matches,
  deliveries,
  "mosteconomicalbowlerinsuperovers",
  ipl.economicalBowlerInSuperOver
);
createOutputFile(
  matches,
  deliveries,
  "strikerateofabatsman(ViratKohli)perseason",
  ipl.strikerateOfABatsmanPerSeason
);
createOutputFile(
  matches,
  deliveries,
  "playeswithmostnumberofmanofthematchawardsperseason",
  ipl.playerWithMostMOMPerSeason
);
createOutputFile(
  matches,
  deliveries,
  "highestnumberoftimesbowlerdismissedsamebatsman",
  ipl.highestNumberOfTimesSameBatsmanAndBowlerInvolvedInDismissal
);

function createOutputFile(matches, deliveries, outputFileName, cb) {
  const result = cb(matches, deliveries);
  const outputFilePath = path.join(
    __dirname,
    `../public/output/${outputFileName}.json`
  );

  fs.writeFile(outputFilePath, JSON.stringify(result), (err, data) => {
    if (err) {
      console.error(err);
    }
  });
}
