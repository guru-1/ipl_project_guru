const csv = require("csvtojson");
const fs = require("fs");
const path = require("path");

//CSV file paths

const deliveriesFile = "./data/deliveries.csv";
const matchesFile = "./data/matches.csv";

function convertCsvToJson(csvFilePath) {
  const absoluteCsvFilePath = path.join(__dirname, csvFilePath);
  csv()
    .fromFile(absoluteCsvFilePath)
    .then((jsonObj) => {
      const JsonFilePath = renamePath(absoluteCsvFilePath);

      writeToJson(JsonFilePath, jsonObj);
    });
}

function renamePath(path) {
  return path.replace(".csv", ".json");
}

function writeToJson(JsonFilePath, data) {
  fs.writeFile(JsonFilePath, JSON.stringify(data), function (err, data) {
    if (err) {
      console.error(err);
    }
  });
}

convertCsvToJson(deliveriesFile);
convertCsvToJson(matchesFile);
